public class Tchebychev {
	static Polynome[] polynomes = new Polynome[10];
	
	public static void main(String[] args) {
		Polynome p0 = new Polynome();
		p0.setMonome(new Monome(1F, 0));
		polynomes[0] = p0;
		
		Polynome p1 = new Polynome();
		p1.setMonome(new Monome(1F, 1));
		polynomes[1] = p1;
		
		for(int i = 2; i < polynomes.length; i++) {
			Polynome p = new Polynome();
			p.setMonome(new Monome(2F, 1));
			polynomes[i] = polynomes[i-1].produit(p).difference(polynomes[i-2]);
		}
		
		for(Polynome p : polynomes) System.out.println(p);
	}
}

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class TestPolynome {
	private Polynome[] hermite = new Polynome[10];

	@Before
	public void setUp() {
		for(int i = 0; i < hermite.length; i++) hermite[i] = new Polynome();
		hermite[0].setMonome(new Monome(1F, 0));

		hermite[1].setMonome(new Monome(1F, 1));

		hermite[2].setMonome(new Monome(1F, 2));
		hermite[2].setMonome(new Monome(-1F, 0));

		hermite[3].setMonome(new Monome(1F, 3));
		hermite[3].setMonome(new Monome(-3F, 1));

		hermite[4].setMonome(new Monome(1F, 4));
		hermite[4].setMonome(new Monome(-6F, 2));
		hermite[4].setMonome(new Monome(3F, 0));

		hermite[5].setMonome(new Monome(1F, 5));
		hermite[5].setMonome(new Monome(-10F, 3));
		hermite[5].setMonome(new Monome(15F, 1));

		hermite[6].setMonome(new Monome(1F, 6));
		hermite[6].setMonome(new Monome(-15F, 4));
		hermite[6].setMonome(new Monome(45F, 2));
		hermite[6].setMonome(new Monome(-15F, 0));

		hermite[7].setMonome(new Monome(1F, 7));
		hermite[7].setMonome(new Monome(-21F, 5));
		hermite[7].setMonome(new Monome(105F, 3));
		hermite[7].setMonome(new Monome(-105F, 1));

		hermite[8].setMonome(new Monome(1F, 8));
		hermite[8].setMonome(new Monome(-28F, 6));
		hermite[8].setMonome(new Monome(210F, 4));
		hermite[8].setMonome(new Monome(-420F, 2));
		hermite[8].setMonome(new Monome(105F, 0));

		hermite[9].setMonome(new Monome(1F, 9));
		hermite[9].setMonome(new Monome(-36F, 7));
		hermite[9].setMonome(new Monome(378F, 5));
		hermite[9].setMonome(new Monome(-1260F, 3));
		hermite[9].setMonome(new Monome(945F, 1));	
	}

	@Test
	public void testtoStringPolynomeNul() {
		Polynome nul = new Polynome();
		assertEquals("0", nul.toString());
	}

	@Test
	public void testtoStringPolynomeCoefficientsPositifs() {
		Polynome p = new Polynome();
		p.setMonome(new Monome(8F, 4));
		p.setMonome(new Monome(8F, 2));
		p.setMonome(new Monome(1F, 0));
		assertEquals("8.0xe4 + 8.0xe2 + 1.0", p.toString());
	}

	@Test
	public void testtoStringPolynomeCoefficientsPositifsEtNgatifs() {
		Polynome p = new Polynome();
		p.setMonome(new Monome(128F, 8));
		p.setMonome(new Monome(160F, 4));
		p.setMonome(new Monome(-32F, 2));
		p.setMonome(new Monome(1F, 0));
		assertEquals("128.0xe8 + 160.0xe4 - 32.0xe2 + 1.0", p.toString());
	}

	@Test
	public void testtoStringPolynomePremierCoefficientNgatif() {
		Polynome p = new Polynome();
		p.setMonome(new Monome(-128F, 8));
		p.setMonome(new Monome(160F, 4));
		p.setMonome(new Monome(-32F, 2));
		p.setMonome(new Monome(1F, 0));
		assertEquals("-128.0xe8 + 160.0xe4 - 32.0xe2 + 1.0", p.toString());
	}

	@Test
	public void testDerivePolynome() {
		Polynome p = new Polynome();
		p.setMonome(new Monome(2F, 2));
		p.setMonome(new Monome(6F, 1));
		p.setMonome(new Monome(32F, 0));
		assertEquals("4.0x + 6.0", p.drive().toString());
	}

	@Test
	public void testSommePolynomes() {
		Polynome p = new Polynome();
		p.setMonome(new Monome(-128F, 8));
		p.setMonome(new Monome(160F, 4));
		p.setMonome(new Monome(-32F, 2));
		p.setMonome(new Monome(1F, 0));
		Polynome other = new Polynome();
		other.setMonome(new Monome(128F, 8));
		other.setMonome(new Monome(6F, 4));
		other.setMonome(new Monome(33F, 3));
		assertEquals("166.0xe4 + 33.0xe3 - 32.0xe2 + 1.0",
				p.somme(other).toString());
	}

	@Test
	public void testProduitPolynomeParMonome() {
		Polynome p = new Polynome();
		p.setMonome(new Monome(128F, 8));
		p.setMonome(new Monome(160F, 4));
		p.setMonome(new Monome(-32F, 2));
		p.setMonome(new Monome(1F, 0));
		Monome m = new Monome(2F, 1);
		assertEquals("256.0xe9 + 320.0xe5 - 64.0xe3 + 2.0x",
				p.produitMonome(m).toString());
	}
	
	@Test
	public void testDegre() {
		Polynome p = new Polynome();
		p.setMonome(new Monome(128F, 8));
		p.setMonome(new Monome(160F, 4));
		p.setMonome(new Monome(-32F, 2));
		p.setMonome(new Monome(1F, 0));
		assertEquals(8, p.degr());
	}
	
	@Test
	public void testDifference() {
		Polynome p1 = new Polynome();
		p1.setMonome(new Monome(128F, 1));
		p1.setMonome(new Monome(160F, 2));
		p1.setMonome(new Monome(-32F, 3));
		p1.setMonome(new Monome(1F, 4));
		
		Polynome p2 = new Polynome();
		p2.setMonome(new Monome(64F, 1));
		p2.setMonome(new Monome(80F, 2));
		p2.setMonome(new Monome(-16F, 3));
		p2.setMonome(new Monome(1F, 4));
		
		assertEquals("-16.0xe3 + 80.0xe2 + 64.0x", p1.difference(p2).toString());
	}

	@Test
	public void testHermiteRecurrenceDerivee() {
		for(int i = 1; i < hermite.length; i++)
			assertEquals(
					hermite[i].drive().toString(), 
					hermite[i-1].produitMonome(new Monome(i, 0)).toString()
					);
	}
	
	@Test
	public void testHermiteRecurrenceDifferentielle() {
		for(int i = 0; i < hermite.length; i++) {
			Polynome ddh = hermite[i].drive().drive();
			Polynome dxh = hermite[i].drive().produitMonome(new Monome(-1F, 1));
			Polynome dnh = hermite[i].produitMonome(new Monome((float)i, 0));
			assertEquals("0", ddh.somme(dxh).somme(dnh).toString());
		}
	}
	
	@Test
	public void testProduit() {
		Polynome p1 = new Polynome();
		p1.setMonome(new Monome(-3F, 2));
		p1.setMonome(new Monome(4F, 1));
		p1.setMonome(new Monome(-2F, 0));
		
		Polynome p2 = new Polynome();
		p2.setMonome(new Monome(1F, 3));
		p2.setMonome(new Monome(-1F, 1));
		p2.setMonome(new Monome(1F, 0));
		
		assertEquals("-3.0xe5 + 4.0xe4 + xe3 - 7.0xe2 + 6.0x - 2.0", p1.produit(p2).toString());
	}
}

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestTchebychev {
	private Polynome[] polynomes = new Polynome[10];
	
	@Before
	public void setUp() throws Exception {
		Polynome p0 = new Polynome();
		p0.setMonome(new Monome(1F, 0));
		polynomes[0] = p0;
		
		Polynome p1 = new Polynome();
		p1.setMonome(new Monome(1F, 1));
		polynomes[1] = p1;
		
		for(int i = 2; i < polynomes.length; i++) {
			Polynome p = new Polynome();
			p.setMonome(new Monome(2F, 1));
			polynomes[i] = polynomes[i-1].produit(p).difference(polynomes[i-2]);
		}
	}

	@After
	public void tearDown() throws Exception {
		for(int i = 0; i < polynomes.length; i++) {
			polynomes[i] = null;
		}
	}

	@Test
	public void testP1() {
		for(int i = 0; i < polynomes.length; i++) {
			assertEquals(i, polynomes[i].degr());
		}
	}
	
	@Test
	public void testP2() {
		Polynome p1 = new Polynome();
		p1.setMonome(new Monome(-1F, 2));
		p1.setMonome(new Monome(1F, 0));
		
		Monome x = new Monome(1F, 1);
		
		for(int i = 0; i < polynomes.length; i++) {
			Monome nc = new Monome(i*i, 0);
			
			Polynome t = polynomes[i];
			Polynome td = t.drive();
			Polynome tdd = td.drive();
			
			Polynome tn = p1.produit(tdd).difference(td.produitMonome(x)).somme(t.produitMonome(nc));
			assertEquals("0", tn.toString());
		}
	}
}

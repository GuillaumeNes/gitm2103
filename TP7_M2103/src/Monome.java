public class Monome {

    /**
     * coefficient du monme
     */
    private float coefficient;

    /**
     * exposant du monme
     */
    private int exposant;

    /**
     * construit un monme
     * 
     * @param coefficient
     *            coefficient du monme
     * @param exposant
     *            exposant du monme
     * @exception IllegalArgumentException
     *                si l'exposant est ngatif
     */
    public Monome(float coefficient, int exposant)
            throws IllegalArgumentException {
        if (exposant < 0) {
            throw new IllegalArgumentException("exposant ngatif");
        }
        this.coefficient = coefficient;
        this.exposant = exposant;
    }

    /**
     * retourne le coefficient d'un monme
     * 
     * @return coefficient
     */
    public float getCoefficient() {
        return this.coefficient;
    }

    /**
     * retourne l'exposant d'un monme
     * 
     * @return exposant
     */
    public int getExposant() {
        return this.exposant;
    }

    /**
     * calcule la somme de deux monmes
     * 
     * @param m
     *            deuxime oprande de la somme
     * @return monme rsultat
     * @exception ArithmeticException
     *                si les exposants des 2 monmes ne sont pas les mmes
     */
    public Monome somme(Monome m) throws ArithmeticException {
        if (this.getExposant() != m.getExposant()) {
            throw new ArithmeticException(
                    "coefficient des deux monmes diffrents");
        }
        return new Monome(this.getCoefficient() + m.getCoefficient(),
                this.getExposant());
    }

    /**
     * calcule le produit de deux monmes
     * 
     * @param m
     *            deuxime oprande du produit
     * @return monme rsultat
     */
    public Monome produit(Monome m) {
        return new Monome(this.getCoefficient() * m.getCoefficient(),
                this.getExposant() + m.getExposant());
    }

    /**
     * calcule la drive d'un monme
     * 
     * @return monme rsultat
     */
    public Monome drive() {
        if (this.getExposant() == 0) {
            return new Monome(0.0F, 0);
        } else {
            return new Monome(this.getCoefficient() * this.getExposant(),
                    this.getExposant() - 1);
        }
    }

    /**
     * produit une version unicode d'un monme
     * 
     * @return chane rsultat
     */
    @Override
    public String toString() {
        if (this.estNul()) {
            return "0";
        }
        String resultat = "";
        if (this.getCoefficient() < 0.0F) {
            resultat += " - ";
            if ((this.getCoefficient() != -1.0F)
                    || (this.getCoefficient() == -1.0F
                            && this.getExposant() == 0)) {
                resultat += (-this.getCoefficient());
            }
        } else {
            resultat += " + ";
            if ((this.getCoefficient() != 1.0F)
                    || (this.getCoefficient() == 1.0F
                            && this.getExposant() == 0)) {
                resultat += this.getCoefficient();
            }
        }
        if (this.getExposant() == 0) {
            return resultat;
        }
        resultat += "x";
        if (this.getExposant() != 1) {
            resultat += "e" + this.getExposant();
        }
        return resultat;
    }

    /**
     * teste si un monme est nul
     * 
     * @return true si le monme est nul
     */
    public boolean estNul() {
        return this.getCoefficient() == 0;
    }

}

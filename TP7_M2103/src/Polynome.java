public class Polynome {

	/**
	 * degr maximum du polynme
	 */
	public static final int DEGRE_MAX = 99;
	/**
	 * tableau contenant les coefficients des monmes du polynme
	 */
	private float[] coefficients;

	/**
	 * cre un polynme nul
	 */
	public Polynome() {
		// crer le tableau des coefficients
		this.coefficients = new float[Polynome.DEGRE_MAX + 1];
		// mettre  0 tous les coefficients
		for (int i = 0; i < this.coefficients.length; i++) {
			this.coefficients[i] = 0.0F;
		}
	}

	/**
	 * positionne un nouveau monme dans un polynme
	 * 
	 * @param m
	 *            monme  positionner dans le polynme
	 * @throws IllegalArgumentException
	 *             si l'exposant du monme est suprieur  DEGRE_MAX
	 */
	public void setMonome(Monome m) throws IllegalArgumentException {
		if (m.getExposant() > Polynome.DEGRE_MAX) {
			throw new IllegalArgumentException("exposant du monome trop grand "
					+ m.getExposant() + " " + m);
		}
		this.coefficients[m.getExposant()] = m.getCoefficient();
	}

	/**
	 * accde  un monme du polynme
	 * 
	 * @param exposant
	 *            exposant du monme  accder
	 * @throws IllegalArgumentException
	 *             si l'exposant du monme n'est pas compris dans [0,DEGRE_MAX]
	 */
	public Monome getMonome(int exposant) throws IllegalArgumentException {
		if (exposant < 0 || exposant > Polynome.DEGRE_MAX) {
			throw new IllegalArgumentException("exposant invalide " + exposant);
		}
		return new Monome(this.coefficients[exposant], exposant);
	}

	/**
	 * calcule la drive d'un polynme
	 * 
	 * @return drive du polynme
	 */
	public Polynome drive() {
		Polynome p = new Polynome();
		for(int i = 0; i < DEGRE_MAX; i++) {
			p.setMonome(this.getMonome(i).drive());
		}
		return p;
	}

	/**
	 * calcule la somme de deux polynmes
	 * 
	 * @param p
	 *            deuxime polynme associ  la somme
	 * @return polynme resultat
	 */
	public Polynome somme(Polynome p) {
		Polynome pn = new Polynome();
		for(int i = 0; i < DEGRE_MAX; i++) {
			pn.setMonome(this.getMonome(i).somme(p.getMonome(i)));
		}
		return pn;
	}

	/**
	 * calcule le produit d'un polynme et d'un monme
	 * 
	 * @param m
	 *            monme associ au produit
	 * 
	 * @throws IllegalArgumentException
	 *             si l'exposant d'un monme du rsultat est suprieur 
	 *             DEGRE_MAX
	 * @return polynme produit
	 */
	public Polynome produitMonome(Monome m) throws IllegalArgumentException {
		Polynome p = new Polynome();
		for(int i = 0; i < Polynome.DEGRE_MAX; i++) {
			p.setMonome(this.getMonome(i).produit(m));
		}
		return p;
	}
	
	/**
	 * calcule le produit de deux polynmes
	 * @param p
	 * 			polynme associ au produit
	 * @throws IllegalArgumentException
	 * 			si l'exposant d'un monme du polynme rsultant est suprieur  
	 * 			DEGRE_MAX
	 */
	public Polynome produit(Polynome p) {
		Polynome pn = new Polynome();
		for(int x = 0; x < Polynome.DEGRE_MAX; x++) {
			if(!this.getMonome(x).estNul()) {
				for(int y = 0; y < Polynome.DEGRE_MAX; y++) {
					if(!p.getMonome(y).estNul()) {
						Monome m = this.getMonome(x).produit(p.getMonome(y));
						if(pn.getMonome(m.getExposant()).estNul()) {
							pn.setMonome(m);
						} else {
							pn.setMonome(pn.getMonome(m.getExposant()).somme(m));
						}
					}
				}
			}
		}
		return pn;
	}
	
	/**
	 * Retourne le degr du polynme
	 * @return le degr du polynme
	 */
	public int degr() {
		for(int i = Polynome.DEGRE_MAX - 1; i >= 0; i--) {
			if(this.coefficients[i] != 0F) return i;
		}
		return 0;
	}
	
	/**
	 * Calcule la diffrence entre deux polynmes
	 * @param p
	 * 			polynome  soustraire
	 * @return la diffrence entre les deux polynmes
	 */
	public Polynome difference(Polynome p) {
		return this.somme(p.produitMonome(new Monome(-1F, 0)));
	}

	@Override
	public String toString() {
		String rsultat = "";
		for (int i = this.coefficients.length - 2; i >= 0; i--) {
			if (!this.getMonome(i).estNul()) {
				rsultat += this.getMonome(i);
			}
		}
		if (rsultat.equals("")) {
			return "0";
		}
		if (rsultat.charAt(1) == '-') {
			return "-" + rsultat.substring(3, rsultat.length());
		}
		return rsultat.substring(3, rsultat.length());

	}

}

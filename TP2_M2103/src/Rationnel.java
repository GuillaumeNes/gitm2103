public class Rationnel {

	private long n;
	private long d;
	
	public Rationnel(long n, long d) throws IllegalArgumentException {
		if (d == 0) throw new IllegalArgumentException("Le d�nominateur ne doit pas �tre �gale � 0");
		this.n = n;
		this.d = d;
	}
	
	public long getNumerateur() {
		return this.n;
	}
	
	public long getDenominateur() {
		return this.d;
	}
	
	public Rationnel reduction() {
		long pgcdComm = pgcd(this.n, this.d);
		return new Rationnel(this.n/pgcdComm, this.d/pgcdComm);
	}
	
	public Rationnel somme(Rationnel r) {
		return new Rationnel(this.n*r.d + r.n*this.d, this.d*r.d);
	}
	
	public Rationnel produit(Rationnel r) {
		return new Rationnel(this.n*r.n, this.d*r.d);
	}
	
	public Rationnel division(Rationnel r) {
		
		return new Rationnel(this.n*r.d, this.d*r.n);
		
	}
	
	public String toString() {
		Rationnel r = this.reduction();
		return r.n + "/" + r.d;
	}
	
	public static long pgcd(long a, long b) {
		while (a != b) {
			if (a > b) a -= b;
			else b -= a;
		}
		return a;
	}

}

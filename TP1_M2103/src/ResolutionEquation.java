import java.util.Scanner;

public class ResolutionEquation {

	public static void main(String[] args) {

		System.out.println("Coefficient a :");
		Scanner entr�e = new Scanner(System.in);
		int a = entr�e.nextInt();
		
		while (a == 0) {
			System.out.println("Attention a doit �tre diff�rent de 0 : ");
			a = entr�e.nextInt();
		}
		
		System.out.println("Coefficient b :");
		int b = entr�e.nextInt();
		
		System.out.println("Entrer la valeur du coefficient c : ");
		int c = entr�e.nextInt();
		
		System.out.println("R�solution de l'�quation : " + a + "x2 + " + b + "x + " + c + " = 0");
		entr�e.close();
		
		double discriminant = (b*b) - ((4*a) * c);
		System.out.println("Discriminant : " + discriminant);
		
		if (discriminant == 0.0) {
			double x1 = ((b * -1) / ( a * 2));
			System.out.println("Les solutions sont " + x1 + " et " + x1);
		}
		
		if (discriminant > 0.0) {
			double x1 = (double) ((b * -1) + (Math.sqrt(b)) / ( 2 * a));
			double x2 = (double) (((b * -1) * (Math.sqrt(b)) / (2 * a)));
			System.out.println("Les solutions sont " + x1 + " et " + x2);
		}
		
		if (discriminant < 0.0) {
			System.out.println("Aucune solution r�el");
		}

	}

}

import java.util.Scanner;

public class Asterisques {

	public static void main(String[] args) {

		char etoile = '*';
		System.out.println("Saisir n nombre �toiles : ");
		Scanner entr�e = new Scanner(System.in);
		
		int n = entr�e.nextInt();
		
		entr�e.close();
		
		int compteur = n;

		while (compteur > 0) {
			for (int i = 1; i <= compteur; i++) {
				System.out.println(etoile);
			}
			compteur = compteur - 1;
		}
	}

}

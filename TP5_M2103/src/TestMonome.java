import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestMonome {
	private Monome m1, m2;
	
	private static final float A = 5F;
	private static final float B = 3F;
	
	@Before
	public void setUp() throws Exception {
		this.m1 = new Monome(A, (int) B);
		this.m2 = new Monome(B, (int) A);
	}

	@After
	public void tearDown() throws Exception {
		this.m1 = null;
		this.m2 = null;
	}
	 
	@Test
	public void testMonome() {
		Monome m = new Monome(A, (int) B);
		assertEquals(A, m.getCoefficient(), 0);
		assertEquals((int) B, m.getExposant());
	}

	@Test
	public void testCoefficient() {
		assertEquals(A, m1.getCoefficient(), 0);
	}
	
	@Test
	public void testExposant() {
		assertEquals((int) B, m1.getExposant());
	}
	
	@Test
	public void testEstNul() {
		assertTrue(new Monome(0F, 0).estNul());
	}
	
	@Test
	public void testNonEstNul() {
		assertFalse(new Monome(1F, 0).estNul());
	}

	@Test
	public void testSomme() {
		Monome sum = m1.somme(new Monome(4F, 3));
		assertEquals(sum.getCoefficient(), A + 4F, 0);
		assertEquals(sum.getExposant(), (int) B);
	}
	
	@Test
	public void testProduit() {
		Monome mul = m1.produit(m2);
		assertEquals(mul.getCoefficient(), A * B, 0);
		assertEquals(mul.getExposant(), (int) A + B, 0);
	}
	
	@Test
	public void testDerivee() {
		Monome der = m1.d�riv�e();
		assertEquals(der.getCoefficient(), A * B, 0);
		assertEquals(der.getExposant(), (int)B-1);
	}
	
	@Test
	public void testDeriveeExposantNul() {
		Monome dern = new Monome(A, 0).d�riv�e();
		assertEquals(dern.getCoefficient(), 0F, 0);
		assertEquals(dern.getExposant(), 0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testMonomeException() {
		new Monome(A, -(int)B);
	}
	
	@Test(expected = ArithmeticException.class)
	public void testSommeException() {
		m1.somme(m2);
	}
	
	@Test
	public void testToString() {
		assertEquals("5.0xe3", m1.toString());
	}
}

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class TestBanque {
	private Banque b;
	private CompteBancaire c1;
	private CompteBancaire c2;
	
	@Before
	public void setUp() throws Exception {
		b = new Banque("Soci�t� G�n�rale");
		c1 = new CompteBancaire("777");
		c2 = new CompteBancaire("123");
	}
	
	@After
	public void tearDown() throws Exception{
		b = null;
		c1 = null;
		c2 = null;
	}
	
	
	@Test
	public void testBanque() {
		assertEquals(b.getLibell�(), new Banque("Soci�t� G�n�rale").getLibell�());
	}
	
	@Test
	public void testGetLibell�() {
		assertEquals("Soci�t� G�n�rale", b.getLibell�());
	}
	
	@Test
	public void testOuvrir() {
		b.ouvrir("777" , 25.0F);
		assertTrue(b.estCompteExistant("777"));
		assertEquals(25.0F, b.getCompte("777").solde(), 0);
	}
	
	@Test
	public void testFermer() {
		b.ouvrir("123", 0.0F);
		b.fermer("123");
		assertFalse(b.estCompteExistant("123"));
	}
	
	@Test
	public void testD�poser() {
		b.ouvrir(c1.getNum�ro(), 0.0F);
		b.d�poser(c1.getNum�ro(), 10.0F);
		assertEquals(10.0F, b.getCompte(c1.getNum�ro()).solde(), 0);
	}
	
	@Test
	public void testRetirer() {
		b.ouvrir(c1.getNum�ro(), 20.0F);
		b.retirer(c1.getNum�ro(), 5.0F);
		assertEquals(15.0F, b.getCompte(c1.getNum�ro()).solde(), 0);
	}
	
	@Test
	public void testGetCompte() {
		b.ouvrir(c1.getNum�ro(), 10.0F);
		assertEquals(c1, b.getCompte(c1.getNum�ro()));
	}
	
	@Test
	public void testEstExistant() {
		b.ouvrir(c1.getNum�ro(), 0.0F);
		assertTrue(b.estCompteExistant(c1.getNum�ro()));
		assertFalse(b.estCompteExistant(c2.getNum�ro()));
	}
	
	public void testToString() {
		Banque sg = new Banque("SG");
		c1 = sg.ouvrir(c1.getNum�ro(), 255.0F);
		c2 = sg.ouvrir(c2.getNum�ro(), 750.0F);
		String expected = "[Banque : " + sg.getLibell�() + "\n" + c1 + "\n" + c2 + "]";
		System.out.println(sg);
		System.out.println(expected);
		assertEquals(expected, sg.toString());
	}
}

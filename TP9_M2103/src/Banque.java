import java.util.HashMap;
import java.util.Map;


public class Banque {
	
	/**
	 * Liste des comptes bancaires
	 */
	private Map<String, CompteBancaire> comptes = new HashMap<>();

	/**
	 * Libell� d'une Banque
	 */
	private String libell�;
	
	
	/**
	 * Contruit une Banque
	 * @param libell�
	 * 			le libell� d'une Banque
	 */
	public Banque(String libell�) {
		this.libell� = libell�;
	}
	
	
	/**
	 * Retourne le libell� de la Banque
	 * @return
	 * 			le libell� de la Banque
	 */
	public String getLibell�() {
		return this.libell�;
	}
	
	
	/**
	 * Ouvre un compte Bancaire muni d'un code et d'un solde de d�part
	 * @param code
	 * 			Num�ro du compte
	 * @param montant
	 * 			Montant de d�part pour le compte
	 * @return
	 * 
	 * @throws IllegalArgumentException
	 * 			Lorsque le compte existe d�j� ou si le montant initial est n�gatif
	 */
	public CompteBancaire ouvrir(String code, float montant) throws IllegalArgumentException {
		if(estCompteExistant(code)) {
			throw new IllegalArgumentException("Ce compte existe d�j� !");
		}
		if(montant < 0.0f) {
			throw new IllegalArgumentException("Montant n�gatif !");
		}
		CompteBancaire c = new CompteBancaire(code);
		c.d�poser(montant);
		comptes.put(code, c);
		return(c);
	}
	
	
	/**
	 * Ferme un comte bancaire
	 * @param code
	 * 			Num�ro du compte � fermer
	 * @throws IllegalArgumentException
	 * 			Lorsque le compte n'existe pas ou que le solde est non nul
	 */
	public void fermer(String code) throws IllegalArgumentException{
		if(!estCompteExistant(code)) {
			throw new IllegalArgumentException("Le compte donn� n'existe pas !");
		}
		CompteBancaire c = getCompte(code);
		if(c.solde() != 0.0F) {
			throw new IllegalArgumentException("Le solde est non nul !");
		}
		comptes.remove(code);
	}
	
	
	public CompteBancaire d�poser(String code, float montant) throws IllegalArgumentException{
		if(!estCompteExistant(code)) {
			throw new IllegalArgumentException("Le compte n'existe pas !");
		}
		if(montant <= 0.0F) {
			throw new IllegalArgumentException("Solde n�gatif ou null !");
		}
		CompteBancaire c = getCompte(code);
		c.d�poser(montant);
		return c;
	}
	
	
	/**
	 * Permet de retirer de l'argent d'un compte
	 * @param code
	 * 			Num�ro de compte
	 * @param montant
	 * 			Montant � retirer
	 * @throws IllegalArgumentException
	 * 			Lorsque le compte n'existe pas ou que le solde est n�gatif/nul
	 */
	public void retirer(String code, float montant) throws IllegalArgumentException{
		if(!estCompteExistant(code)) {
			throw new IllegalArgumentException("Le compte donn� n'existe pas !");
		}
		if(montant <= 0.0F) {
			throw new IllegalArgumentException("Le montant est n�gatif ou nul !");
		}
		CompteBancaire c = getCompte(code);
		c.retirer(montant);
	}
	
	
	/**
	 * Renvoie un Compte Bancaire � partir du num�ro de compte donn�
	 * @param code
	 * 			Num�ro de compte
	 * @return
	 * 			Le compte si trouv�, sinon rien
	 */
	public CompteBancaire getCompte(String code) {
		return comptes.get(code);
	}
	
	
	/**
	 * Permet de savoir si un compte existe dans la banque
	 * @param code
	 * 			Num�ro du compte
	 * @return
	 * 			True si le compte existe dans la banque, sinon false
	 */
	public boolean estCompteExistant(String code) {
		return comptes.containsKey(code);
	}
	
	
	/**
	 * Affiche les comptes de la banque ayant un solde n�gatif
	 */
	public void afficherComptesDebiteurs() {
		for(CompteBancaire cb : comptes.values()) {
			if(cb.solde() < 0.0F) {
				System.out.println(cb);
			}
		}
	}
	
	
	/**
	 * Repr�sente la banque sous la forme d'une cha�ne de caract�res
	 * @return
	 * 			Le banque sous forme de cha�ne de caract�res
	 */
	@Override
	public String toString() {
		String str = "[Banque : " + this.libell�;
		for(CompteBancaire cb : comptes.values()) {
			str += "\n"+cb;
		}
		str += "]+";
		return str;
	}
}

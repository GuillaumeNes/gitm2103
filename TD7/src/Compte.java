
public class Compte {
	
	private float credit;
	private float debit;
	
	public Compte() {
		this.credit = 0.0F;
		this.debit = 0.0F;
	}
	
	public float getSolde() {
		return credit - debit;		
	}
	
	public Compte deposer(float v) throws IllegalArgumentException{
		if(v < 0) {
			throw new IllegalArgumentException("valeur inf � 0");
		}
		this.credit += v;
		return this;
	}
	
	public Compte retirer(float v) throws IllegalArgumentException{
		if(v < 0) {
			throw new IllegalArgumentException("Valeur inf � 0");
		}
		this.credit -= v;
		return this;
	}
	
	@Override
	public String toString() {
		return ("Cr�dit: " + this.credit + ", D�bit: " + this.debit);
	}
}

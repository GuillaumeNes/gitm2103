
public class CompteEpargne extends CompteBancaire{
	
	private float taux;
	
	public CompteEpargne(String numero, int taux) throws IllegalArgumentException {
		super(numero);
		if(taux < 0) throw new IllegalArgumentException("Taux n�gatif");
		this.taux = taux;
	}
	
	public float getTaux() {
		return this.taux;
	}
	
	public float interets() {
		return this.getSolde() > 0 ? this.getSolde() * this.getTaux() : 0F;
	}
	
	public CompteEpargne ajouterInterets() {
		return (CompteEpargne) this.deposer(this.interets());
	}
	
	@Override
	public String toString() {
		return super.toString() + ", Taux: " + this.taux;
	}
}

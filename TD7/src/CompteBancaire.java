
public class CompteBancaire extends Compte{
	private String numero;
	
	public CompteBancaire(String numero) {
		this.numero = numero;
	}
	
	public String getNumero() {
		return this.numero;
	}
	
	@Override
	public String toString() {
		return ("Num�ro: " + this.numero + ", Cr�dit: " + super.toString());
	}
	
	public Boolean equals(CompteBancaire cb) {
		return this.getNumero().equals(cb.getNumero());
	}
}

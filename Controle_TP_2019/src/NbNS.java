
public class NbNS {
	private double mantisse;
	private int exposant;
	
	public NbNS(double mantisse, int exposant) throws IllegalArgumentException {
		if(mantisse < 1 || mantisse > 1) {
			throw new IllegalArgumentException("Mantisse Trop grand ou petite");
		}
		this.mantisse = mantisse;
		this.exposant = exposant;
	}
	
	public double getMantisse() {
		return this.mantisse;
	}
	
	public int getExposant() {
		return this.exposant;
	}
	
	public NbNS ordreGrandeur() {
		if (this.mantisse > 5) {
			this.exposant += 1;
			this.mantisse = 1;
		} else {
			this.mantisse = 1;
		}
		return this;
	}
	
	public NbNS produit (NbNS n2) {
		if (this.mantisse * n2.mantisse == 0) {
			this.mantisse *= n2.mantisse;
			this.exposant += n2.exposant;
		} else {
			this.mantisse = this.mantisse * n2.mantisse / 10;
			this.exposant += n2.exposant +1;
		}
		return this;
	}
	
}

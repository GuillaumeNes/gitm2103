
public class Polynome {
	
private static final int TAILLE_MAX = 150;
private float [] coefficients;
private int exp;
	
	/**
	 * Construit un polynome vide
	 */
	public Polynome() {
		this.coefficients = new float [Polynome.TAILLE_MAX];
		for (int p=0; p<this.coefficients.length; p++) {
			this.coefficients[p] = 0.0f;
		}
	}
	
	
	/**
	 * R�cup�re le mon�me d'exposant "exposan"t dans le polynome 
	 * @param ex
	 *		Exposant du monome
	 * @return le mon�me d'expostant "exposant
	 * @throws IllegalArgumentException
	 * 		Si l'exposant est inf�rieur � 0
	 */
	public Monome getMonome(int ex) throws IllegalArgumentException{
		if(exp < 0 || exp >= Polynome.TAILLE_MAX) {
			throw new IllegalArgumentException("Erreur");
		}
		return new Monome(this.coefficients[exp], exp);
	}
	
	
	/**
	 * D�finit un monome dans le polynome
	 * @param m
	 * 		le monome � d�finir
	 * @throws IllegalArgumentException
	 * 		si l'exposant est inf�rieur ou �gale � la taille max
	 */
	public void setPolynome(Monome m) throws IllegalArgumentException {
		if(m.getExposant() >= Polynome.TAILLE_MAX) {
			throw new IllegalArgumentException("Erreur");
		}
		this.coefficients[m.getExposant()] = m.getCoefficient();
	}
	
	
	/**
	 * Calcule la somme de deux polynomes
	 * @param polynome
	 * 		polynome � calculer
	 * @return la somme de deux polynomes
	 */
	public Polynome somme(Polynome polynome){
		return null;
	}
	
	
	/**
	 * Calcule le produit entre le polynome et un monome
	 * @param monome
	 * 		Le monome � multiplier
	 * @return le produit entre le polynome et un monome
	 */
	public Polynome produit(Monome monome){
		return null;
	}
	
	
	/**
	 * Calcule la d�riv�e du polynome
	 * @return la d�riv�e du polynome
	 */
	public Polynome derivee() {
		return null;
	}
	
}


public class Dur�e {

	private final int h, m, s;
	
	public Dur�e(int h, int m, int s) throws IllegalArgumentException {
		if (h < 0 || m >= 60 || m < 0 || s >= 60 || s < 0) {
			throw new IllegalArgumentException();
		}
		this.h = h;
		this.m = m;
		this.s = s;
	}

	public int getHeures() {
		return this.h;
	}
	
	public int getMinutes() {
		return this.m;
	}
	
	public int getSecondes() {
		return this.s;
	}
	
	public boolean egal(Dur�e d) {
		return(this.h == d.h && this.m == d.m && this.s == d.s);
	}
	
	public boolean inf(Dur�e d) {
		return(this.h < d.h || (this.h == d.h && this.m < d.m) || (this.h == d.h && this.m == d.m && this.s < d.s));
	}

	public Dur�e ajouterUneSeconde() {
		if(this.getSecondes() < 59) {
			return new Dur�e(this.getHeures(), this.getMinutes(), this.getSecondes()+1);
		} else if(this.getMinutes() < 59) {
			return new Dur�e(this.getHeures(), this.getMinutes()+1, 0);
		} else {
			return new Dur�e(this.getHeures()+1, 0, 0);
		}
	}

	
	public String toString() {
		return this.h + ":" + this.m + ":" + this.s;
	}
}
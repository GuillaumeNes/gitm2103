import static org.junit.Assert.*;

import org.junit.Test;

public class TestCouleur {
	
	private static final int r = 10;
	private static final int v = 20;
	private static final int b = 30;
	
	private static final int r2 = 15;
	private static final int v2 = 25;
	private static final int b2 = 35;

	@Test
	public void testGetRouge() {
		Couleur c = new Couleur(255, 0, 0);
		assertEquals(Couleur.ROUGE.getRouge(), c.getRouge());
	}

	@Test
	public void testGetVert() {
		Couleur c = new Couleur(0, 255, 0);
		assertEquals(Couleur.VERT.getVert(), c.getVert());
	}

	@Test
	public void testGetBleu() {
		Couleur c = new Couleur(0, 0, 255);
		assertEquals(Couleur.BLEU.getBleu(), c.getBleu());
	}

	@Test
	public void testValeurRVB() {
		Couleur c = new Couleur(r, v ,b);
		assertEquals(r * ((int) Math.pow(256D,  2D)) + v * 256 + b, c.valeurRVB());
	}

	@Test
	public void testSetRouge() {
		Couleur c = new Couleur(r, v, b);
		Couleur c2 = new Couleur(r2, v, b);
		c.setRouge(r2);
		assertEquals(c.getRouge(), c2.getRouge());
	}

	@Test
	public void testSetVert() {
		Couleur c = new Couleur(r, v, b);
		Couleur c2 = new Couleur(r, v2, b);
		c.setVert(v2);
		assertEquals(c.getVert(), c2.getVert());
	}

	@Test
	public void testSetBleu() {
		Couleur c = new Couleur(r, v, b);
		Couleur c2 = new Couleur(r, v, b2);
		c.setBleu(b2);
		assertEquals(c.getBleu(), c2.getBleu());
	}

}

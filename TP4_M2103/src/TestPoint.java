import static org.junit.Assert.*;

import org.junit.Test;

public class TestPoint {
	
	private static final float a = 5.0F;
	private static final float o = 2.0F;
	

	@Test
	public void testGetAbscisse() {
		Point p = new Point(a, o);
		assertEquals(a, p.getAbscisse(), 0.0F);
	}

	@Test
	public void testGetOrdonnée() {
		Point p = new Point(a, o);
		assertEquals(o, p.getOrdonnée(), 0.0F);
	}

	@Test
	public void testTranslater() {
		Point p = new Point(a, o);
		Point tp = p;
		tp.translater(5, 2);
		assertEquals(p.getAbscisse(), tp.getAbscisse(), 0.0F);
		assertEquals(p.getOrdonnée(), tp.getOrdonnée(), 0.0F);
	}

	@Test
	public void testMettreAEchelle() {
		Point p = new Point(a, o);
		Point tp = p;
		p.mettreAEchelle(3F);
		assertEquals(p.getAbscisse(), tp.getAbscisse(), 0.0F);
		assertEquals(p.getOrdonnée(), tp.getOrdonnée(), 0.0F);
	}

}
